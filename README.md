# README

Este trabalho consiste em um simplificador que a partir das entradas booleanas do usuário te retorna a expressão lógica mais simplificada. Entre diversas 
aplicações, pode ser utilizado para montar circuitos eletrônicos, pois esse software te retorna, além da expressão mais simplificada, quantas portas lógicas foram 
economizadas. Dessa maneira, muitos recursos são poupados.

* Versão do Ruby utilizada: ruby 2.6.3p62

* Versão do Rails utilizada: 5.2.3

* Banco de dados: SQLite

* Gem's: Devise e Bulma rails 0.7.5

* Desenvolvedores:
* Victor Jorge: https://gitlab.com/VictorJorgeFGA
* Maicon Mares: https://gitlab.com/maiconmares
* ...
Destinado ao professor Renato Coral:
Corrigir a branch try_again. Apesar do último commit ser um novo commit, não houve nenhuma adição de funcionalidade. Isso pode ser percebido comparando cada arquivo
com os arquivos presentes nos commits anteriores. Essa sugestão de voltar commits foi feita pelo professor Renato, visto que nosso sistema quebrou após modificações 
finais. Na apresentação em sala mostramos toda a tabela funcionando e também os usuários que foram autenticados por meio
da gem Devise. Porém, o problema que percebemos foi quanto a uma migração que fizemos e alterou a tabela do banco de dados. Assim, estava funcionando apenas em um
de nossos notebooks.